/* BEGIN A3 SETUP */
/*
 * File handles and file tables.
 * New for ASST3
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/unistd.h>
#include <file.h>
#include <syscall.h>


#include <lib.h>
#include <thread.h>
#include <current.h>
#include <vfs.h>
#include <vnode.h>
#include <uio.h>
#include <kern/fcntl.h>
#include <kern/unistd.h>
#include <copyinout.h>
#include <synch.h>



/*** openfile functions ***/

/*
 * file_open
 * opens a file, places it in the filetable, sets RETFD to the file
 * descriptor. the pointer arguments must be kernel pointers.
 * NOTE -- the passed in filename must be a mutable string.
 * 
 * A3: As per the OS/161 man page for open(), you do not need 
 * to do anything with the "mode" argument.
 */
int
file_open(char *filename, int flags, int mode, int *retfd)
{
	// mode is not used this time
	(void) mode; 

	struct vnode *vn;
	struct file *file;
	char filename2[50];
	int result;
	
	if ((flags != O_RDONLY ) && (flags != O_WRONLY) && (flags != O_RDWR)) return EINVAL;

	// each entry in a thread's open file table has a lock called OFT_entry_lock, used to sync the behavior bteween parent and child processes.
	file->lock = lock_create("OFT_entry_lock"); 

	strcpy(filename2, filename);
	result = vfs_open(filename2, flags, 0, &vn);
	if (result) {
		return result;
	}

	file = kmalloc(sizeof(struct file));
	if (file == NULL) {
		vfs_close(vn);
		return -1; // which ERROR?
	}

	file->vn = vn;
	file->ref_count++;
	file->offset = 0;

	// Add file to filetable and return the fd (index)
	result = filetable_addfile(file, retfd);
	if (result) {
		vfs_close(vn);
		kfree(file);
		return EMFILE; // The process's file table was full, or a process-specific limit on open files was reached.
	}

	return 0;
}


/* 
 * file_close
 * Called when a process closes a file descriptor.  Think about how you plan
 * to handle fork, and what (if anything) is shared between parent/child after
 * fork.  Your design decisions will affect what you should do for close.
 */
int
file_close(int fd)
{
	struct file *file;
	int result;

	result = filetable_locatefile(fd, &file);
	if (result) {
		return EBADF; // fd is not a valid file handle.
	}
	file->ref_count--;
	
	// when the last thread close the file, send close request to vfs, then free this open file table entry of current thread.
	if (file->ref_count == 0) { 
		vfs_close(file->vn);
		kfree(file);
	}
	// set filetable[fd] to null
	curthread->t_filetable->openfiles[fd] = NULL;

	return 0;
}

/*** filetable functions ***/

/* 
 * filetable_init
 * pretty straightforward -- allocate the space, set up 
 * first 3 file descriptors for stdin, stdout and stderr,
 * and initialize all other entries to NULL.
 * 
 * Should set curthread->t_filetable to point to the
 * newly-initialized filetable.
 * 
 * Should return non-zero error code on failure.  Currently
 * does nothing but returns success so that loading a user
 * program will succeed even if you haven't written the
 * filetable initialization yet.
 */

int
filetable_init()
{
	int fd;
	char path[50];
	int result;

	KASSERT(curthread->t_filetable == NULL);
	if ((curthread->t_filetable = kmalloc(sizeof (struct filetable)))) {
		return ENOMEM; // Out of memory
	}
	for (fd = 0; fd < __OPEN_MAX; fd++) {
		curthread->t_filetable->openfiles[fd] = NULL;
	}

	strcpy(path, "con:");
	
	// file table entry 0 which is stdin
	result = file_open(path, O_RDONLY, 0, &fd);
	if (result) {
		return result;
	}
	
	// file table entry 1 which is stdout
	result = file_open(path, O_WRONLY, 0, &fd);
	if (result) {
		return result;
	}

	// file table entry which set for stderr
	result = file_open(path, O_WRONLY, 0, &fd);
	if (result) {
		return result;
	}

	return 0;
}	

/*
 * filetable_destroy
 * closes the files in the file table, frees the table.
 * This should be called as part of cleaning up a process (after kill
 * or exit).
 */
int
filetable_destroy(struct filetable *ft)
{
	// if the file table pointer is null, it is either not initialized or been destroyed. So it is already "destroyed" we print a message to indicate this case for debug 
	if (ft == NULL){
		kprintf("Thread trying to free file table before init.\n");
	 	return 1;
	}

	// close every file in this table before free
	for (int fd = 0; fd < __OPEN_MAX; fd++){
		file_close(fd);
	}
        
	kfree(ft);

	return 0;
}	


/* 
 * You should add additional filetable utility functions here as needed
 * to support the system calls.  For example, given a file descriptor
 * you will want some sort of lookup function that will check if the fd is 
 * valid and return the associated vnode (and possibly other information like
 * the current file position) associated with that open file.
 */

int
filetable_addfile(struct file *file, int *retfd)
{
	int fd;
	for (fd = 3; fd < __OPEN_MAX; fd++) { // 0, 1 and 2 are reserved, so we begin from 3
		if (curthread->t_filetable->openfiles[fd] == NULL) {
			curthread->t_filetable->openfiles[fd] = file;
			*retfd = fd;
			return 0;  	// filetable update successful
		}
	}
	return -1; // error. Didn't find space on filetable, error will be returned in file_open
}

/*
 * located file points the pointer file to the crossponding file table entry discribed by fd 
 * If fd is invalid, EBADF is returned. -1 is returned if the pointer file is not setup before calling locate.
 */
int
filetable_locatefile(int fd, struct file **file)
{
	if (fd < 0 || fd > __OPEN_MAX) {
		return EBADF;	// This error message shows fd is not a valid file handle.
	}

	*file = curthread->t_filetable->openfiles[fd];
	if (*file == NULL) {
		return -1;	// *file is not declared before trying to store value into it. 
	}
	return 0;
}


/* END A3 SETUP */
