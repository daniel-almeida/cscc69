/* BEGIN A3 SETUP */
/*
 * Declarations for file handle and file table management.
 * New for A3.
 */

#ifndef _FILE_H_
#define _FILE_H_

#include <kern/limits.h>

struct vnode;

/*
 * file struct
 */

struct file {
	struct vnode *vn;		// each open file table entry has a crossponding vnode
	char filename[NAME_MAX]; 	// file names are useful in debuging
	int flags;		 	// contains O_RDONLY, O_WRONLY and O_RDWR as open flags
	int ref_count;		 	// ref_count keeps track how many threads is opening this filetable entry. send a close to vnode when this decrease to 0.
	off_t offset;		 	// offset is used in file access, parent and child share the same offset
	struct lock* lock;		// a lock to sync behavior of parent and child processes.
};

/*
 * filetable struct
 * just an array, nice and simple.  
 * It is up to you to design what goes into the array.  The current
 * array of ints is just intended to make the compiler happy.
 */
struct filetable {
	struct file *openfiles[OPEN_MAX];
};

/* these all have an implicit arg of the curthread's filetable */
int filetable_init(void);
void filetable_destroy(struct filetable *ft);

/* opens a file (must be kernel pointers in the args) */
int file_open(char *filename, int flags, int mode, int *retfd);

/* closes a file */
int file_close(int fd);

/* A3: You should add additional functions that operate on
 * the filetable to help implement some of the filetable-related
 * system calls.
 */
int filetable_addfile(struct file *file, int *retfd);
int filetable_locatefile(int fd, struct file **file);

#endif /* _FILE_H_ */

/* END A3 SETUP */
