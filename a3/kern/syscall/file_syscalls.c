/* BEGIN A3 SETUP */
/* This file existed for A1 and A2, but has been completely replaced for A3.
 * We have kept the dumb versions of sys_read and sys_write to support early
 * testing, but they should be replaced with proper implementations that 
 * use your open file table to find the correct vnode given a file descriptor
 * number.  All the "dumb console I/O" code should be deleted.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <current.h>
#include <syscall.h>
#include <vfs.h>
#include <vnode.h>
#include <uio.h>
#include <kern/fcntl.h>
#include <kern/unistd.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <copyinout.h>
#include <synch.h>
#include <file.h>
#include <kern/seek.h>

/* This special-case global variable for the console vnode should be deleted 
 * when you have a proper open file table implementation.
 */
struct vnode *cons_vnode=NULL; 

/* This function should be deleted, including the call in main.c, when you
 * have proper initialization of the first 3 file descriptors in your 
 * open file table implementation.
 * You may find it useful as an example of how to get a vnode for the 
 * console device.
 */
void dumb_consoleIO_bootstrap()
{
  int result;
  char path[5];

  /* The path passed to vfs_open must be mutable.
   * vfs_open may modify it.
   */

  strcpy(path, "con:");
  result = vfs_open(path, O_RDWR, 0, &cons_vnode);

  if (result) {
    /* Tough one... if there's no console, there's not
     * much point printing a warning...
     * but maybe the bootstrap was just called in the wrong place
     */
    kprintf("Warning: could not initialize console vnode\n");
    kprintf("User programs will not be able to read/write\n");
    cons_vnode = NULL;
  }
}

/*
 * mk_useruio
 * sets up the uio for a USERSPACE transfer. 
 */
static
void
mk_useruio(struct iovec *iov, struct uio *u, userptr_t buf, 
	   size_t len, off_t offset, enum uio_rw rw)
{

	iov->iov_ubase = buf;
	iov->iov_len = len;
	u->uio_iov = iov;
	u->uio_iovcnt = 1;
	u->uio_offset = offset;
	u->uio_resid = len;
	u->uio_segflg = UIO_USERSPACE;
	u->uio_rw = rw;
	u->uio_space = curthread->t_addrspace;
}

/*
 * sys_open
 * just copies in the filename, then passes work to file_open.
 * You have to write file_open.
 * 
 */
int
sys_open(userptr_t filename, int flags, int mode, int *retval)
{
	char *fname;
	int result;

	if ( (fname = (char *)kmalloc(__PATH_MAX)) == NULL) {
		return ENOMEM;
	}

	result = copyinstr(filename, fname, __PATH_MAX, NULL);
	if (result) {
		kfree(fname);
		return result;
	}

	result =  file_open(fname, flags, mode, retval);
	kfree(fname);
	return result;
}

/* 
 * sys_close
 * You have to write file_close.
 */
int
sys_close(int fd)
{
	return file_close(fd);
}

/* 
 * sys_dup2
 * dup2 only works within the same file table, ie: it only works on two open file table entry of the same thread.
 * dup2 clones the file handle oldfd onto the file handle newfd. If newfd names an open file, that file is closed. 
 * Q> what is retval here for? man page only have oldfd and newfd. 
 */
int
sys_dup2(int oldfd, int newfd, int *retval)
{
	int result;
	
	// Using dup2 to clone a file handle onto itself has no effect, the oldfd is returned without any modification.
	if (newfd == oldfd) return oldfd;

	// checks if newfd and oldfd are valid, if not EBADF will be returned.

	if (oldfd < 0 || oldfd > __OPEN_MAX) {
		return EBADF;	// This error message shows fd is not a valid file handle.
	}
	if (newfd < 0 || newfd > __OPEN_MAX) {
		return EBADF;	// This error message shows fd is not a valid file handle.
	}

	// newfd's file is closed before copy the oldfd value to newfd entry 
	file_close(newfd);

	// update newfd
	curthread->t_filetable->openfiles[newfd] = curthread->t_filetable->openfiles[oldfd]

	// update return
        *retval = newfd;

	return 0;
}

/*
 * sys_read
 * calls VOP_READ.
 * 
 * A3: This is the "dumb" implementation of sys_write:
 * it only deals with file descriptors 1 and 2, and 
 * assumes they are permanently associated with the 
 * console vnode (which must have been previously initialized).
 *
 * In your implementation, you should use the file descriptor
 * to find a vnode from your file table, and then read from it.
 *
 * Note that any problems with the address supplied by the
 * user as "buf" will be handled by the VOP_READ / uio code
 * so you do not have to try to verify "buf" yourself.
 *
 * Most of this code should be replaced.
 */
int
sys_read(int fd, userptr_t buf, size_t size, int *retval)
{
	struct uio user_uio;
	struct iovec user_iov;
	struct file *file;
	int result;
	int offset = 0;
	//struct vnode *vn;

	if (fd < 0 || fd > __OPEN_MAX) {
	  return EBADF;		// error: fd is not a valid file handle.
	}

	result = filetable_locatefile(fd, &file);
	if (result || (file->flags == O_WRONLY) {
		return EBADF; 	// fd is not a valid file handle.
	}
	
	//vn = file->vn;
	
	/* make sure the file being read was properly initialized and contain data */
	if (vn == NULL) {
	  return ENODEV;
	}
	
	//offset = file->offset;

	/* set up a uio with the buffer, its size, and the current offset */
	mk_useruio(&user_iov, &user_uio, buf, size, offset, UIO_READ);

	/* does the read */
	result = VOP_READ(vn, &user_uio);
	if (result) {
		return result;
	}
	file->offset = user_uio.uio_offset;
	/*
	 * The amount read is the size of the buffer originally, minus
	 * how much is left in it.
	 */
	*retval = size - user_uio.uio_resid;

	return 0;
}

/*
 * sys_write
 * calls VOP_WRITE.
 *
 * A3: This is the "dumb" implementation of sys_write:
 * it only deals with file descriptors 1 and 2, and 
 * assumes they are permanently associated with the 
 * console vnode (which must have been previously initialized).
 *
 * In your implementation, you should use the file descriptor
 * to find a vnode from your file table, and then read from it.
 *
 * Note that any problems with the address supplied by the
 * user as "buf" will be handled by the VOP_READ / uio code
 * so you do not have to try to verify "buf" yourself.
 *
 * Most of this code should be replaced.
 */

int
sys_write(int fd, userptr_t buf, size_t len, int *retval) 
{
        struct uio user_uio;
        struct iovec user_iov;
        int result;
        int offset = 0;
		struct vnode *vn;

		if (fd < 0 || fd > __OPEN_MAX) {
		  return EBADF;
		}
		// this function find
		result = filetable_locatefile(fd, &file);

		if (result || (file->flags == O_RDONLY)) {
			return EBADF; // fd is not a valid file handle.
		}
	
		vn = file->vn;
		/* make sure the file being read was properly initialized and contain data */
		if (vn == NULL) {
		  return ENODEV;
		}
	
		//offset = file->offset;

        /* set up a uio with the buffer, its size, and the current offset */
        mk_useruio(&user_iov, &user_uio, buf, len, offset, UIO_WRITE);

        /* does the write */
        result = VOP_WRITE(cons_vnode, &user_uio);
        if (result) {
                return result;
        }

		file->offset = user_uio.uio_offset;

        /*
         * the amount written is the size of the buffer originally,
         * minus how much is left in it.
         */
        *retval = len - user_uio.uio_resid;

        return 0;
}

/*
 * sys_lseek
 * 
 */
int
sys_lseek(int fd, off_t offset, int whence, off_t *retval)
{
	int result;
	struct stat filestat;
        
    if ((whence != SEEK_SET) && (whence != SEEK_CUR) && (whence != SEEK_END)) {
    	return EINVAL;
    }

	if (fd < 0 || fd > __OPEN_MAX) {
		return EBADF;
	}

	result = filetable_locatefile(fd, &file);
	if (result) {
		return EBADF; // fd is not a valid file handle.
	}

	vn = file->vn;
	/* make sure the file being read was properly initialized and contain data */
	if (vn == NULL) {
		return ENODEV;
	}

	if (whence == SEEK_SET) { // new position is offset
		*retval = offset;
	} else {
		if (whence == SEEK_CUR) { // new position is current position + offset
			*retval = file->offset + offset;
		} else {
			if (whence == SEEK_END) { // new position is end of file + offset
				return = VOP_STAT(vn, &filestat);
				if (result) { // see kern/include/kern/stat.h
					return result;
				}
				*retval = filestat->st_size + offset;
			}
		}
	}

	if (result < 0) {
		return EINVAL;
	}
	result = VOP_TRYSEEK(vn, *retval);
	if (result) {
		return result;
	}
	file->offset = *retval;
	return 0;
}


/* really not "file" calls, per se, but might as well put it here */

/*
 * sys_chdir: sets path to the current directory of the current process.
 * 
 */
int
sys_chdir(userptr_t path)
{
	int result;
	
	if (path == NULL) {
		return EFAULT;
	}
	
	result = vfs_chdir(path);
	
	if (result) {
		return result;
	}

	return 0;
}

/*
 * sys___getcwd: stores the name of the current directory in the buffer 
 * and returns (through retval) the lenght of the data stored.
 */
int
sys___getcwd(userptr_t buf, size_t buflen, int *retval)
{
		struct uio cwd_uio;
        struct iovec cwd_iov;
        int result;

        /* set up a uio with the buffer and its size */
        mk_useruio(&cwd_iov, &cwd_uio, buf, len, 0, UIO_READ); //  or uio_init?
        result = vfs_getcwd(&cwd_uio);
		if (result) {
			return -1; // error
		}
		*retval = buflen - cwd_uio.uio_resid;
		if (*retval < 0) { // amount of data stored can not be negative
			return -1;
		}
		return 0;
}


/*
 * sys_fstat: retrieves status information about the file referred to by the file handle fd and stores it in the stat structure pointed to by statptr. 
 */
int
sys_fstat(int fd, userptr_t statptr)
{
	struct stat *filestat;
	struct file *file;


	if (fd < 0 || fd > __OPEN_MAX) {
		return EBADF;
	}

	result = filetable_locatefile(fd, &file);
	if (result) {
		return EBADF; // fd is not a valid file handle.
	}

	/* make sure the file being read was properly initialized and contain data */
	if (file->vn == NULL) {
		return ENODEV;
	}
	result = VOP_STAT(file->vn, filestat); // see kern/include/kern/stat.h
	if (result) {
		return -1;
	}
	statptr = filestat; // not sure about assigning a stat to a userptr_t
	return 0;
}


/*
 * sys_dup2
 * dup2 only works within the same file table, ie: it only works on two open file table entry of the same thread.
 * dup2 clones the file handle oldfd onto the file handle newfd. If newfd names an open file, that file is closed.
 * Q> what is retval here for? man page only have oldfd and newfd.
 */
int
sys_dup2(int oldfd, int newfd, int *retval)
{
    int result;
   
    // Using dup2 to clone a file handle onto itself has no effect, the oldfd is returned without any modification.
    if (newfd == oldfd) return oldfd;

	// check if there is an openfile at filetable[oldfd]
	if (cuthread->t_filetable->openfiles[oldfd] == NULL) return EBADF;

    // checks if newfd and oldfd are valid, if not EBADF will be returned.
    if (oldfd < 0 || oldfd > __OPEN_MAX) {
        return EBADF;    // This error message shows fd is not a valid file handle.
    }
    if (newfd < 0 || newfd > __OPEN_MAX) {
        return EBADF;    // This error message shows fd is not a valid file handle.
    }

    // newfd's file is closed before copy the oldfd value to newfd entry
    result = file_close(newfd);
	if (result) {
		return result;
	}

    // update newfd
    curthread->t_filetable->openfiles[newfd] = curthread->t_filetable->openfiles[oldfd]

    // update return
	*retval = newfd;

    return 0;
}

/* END A3 SETUP */




